#ifndef COUNTRYDIALOG_H
#define COUNTRYDIALOG_H

#include <QDialog>

#include "country.h"

namespace Ui {
class CountryDialog;
}

class CountryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CountryDialog(QWidget *parent = 0);
    ~CountryDialog();

    void SetCountry(Country* pCountry);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::CountryDialog *ui;

    Country* _pCountry;
};

#endif // COUNTRYDIALOG_H
