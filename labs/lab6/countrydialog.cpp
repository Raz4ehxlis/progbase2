#include "countrydialog.h"
#include "ui_countrydialog.h"

#include <QIntValidator>

CountryDialog::CountryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CountryDialog)
{
    ui->setupUi(this);

    QLineEdit* lineEdit = findChild<QLineEdit*>("lineEditPopulation");
    lineEdit->setValidator(new QIntValidator);

    _pCountry = NULL;
}

CountryDialog::~CountryDialog()
{
    delete ui;
}

void CountryDialog::SetCountry(Country* pCountry) {
    _pCountry = pCountry;
}

void CountryDialog::on_buttonBox_accepted()
{
    if (_pCountry == NULL)
        return;

    QLineEdit* lineEdit = findChild<QLineEdit*>("lineEditName");
    _pCountry->SetName(lineEdit->text());

    lineEdit = findChild<QLineEdit*>("lineEditCapital");
    _pCountry->SetCapital(lineEdit->text());

    lineEdit = findChild<QLineEdit*>("lineEditPopulation");
    _pCountry->SetPopulation(lineEdit->text().toInt());

    lineEdit = findChild<QLineEdit*>("lineEditContinent");
    _pCountry->SetContinent(lineEdit->text());
}
