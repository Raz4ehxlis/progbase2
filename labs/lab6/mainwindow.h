#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractItemModel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QListWidget>
#include <QLabel>

#include "country.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_buttonAdd_clicked();

    void on_buttonRemove_clicked();

    void on_buttonFind_clicked();

    void on_listWidgetCountry_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

private:
    Ui::MainWindow *ui;

    QPushButton* _buttonRemove;
    QLineEdit*   _lineEditContinent;
    QListWidget* _listWidgetCountries;
    QTextEdit*   _textEditFindResult;
    QLabel*      _labelNameValue;
    QLabel*      _labelCapitalValue;
    QLabel*      _labelPopulationValue;
    QLabel*      _labelContinentValue;
};

#endif // MAINWINDOW_H
