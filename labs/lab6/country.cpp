#include "country.h"

Country::Country() {
}

Country::Country(const Country& country) {
    _name       = country._name;
    _capital    = country._capital;
    _population = country._population;
    _continent  = country._continent;
}

Country::Country(const QString& name,
                 const QString& capital,
                 int population,
                 const QString& continent) {
    _name       = name;
    _capital    = capital;
    _population = population;
    _continent  = continent;
}

Country::~Country() {
}

QString& Country::GetName() {
    return _name;
}

void Country::SetName(const QString& value) {
    _name = value;
}

QString& Country::GetCapital() {
    return _capital;
}

void Country::SetCapital(const QString& value) {
    _capital = value;
}

int Country::GetPopulation() {
    return _population;
}

void Country::SetPopulation(int value) {
    _population = value;
}

QString& Country::GetContinent() {
    return _continent;
}

void Country::SetContinent(const QString& value) {
    _continent = value;
}

