#ifndef COUNTRY_H
#define COUNTRY_H

#include <QMetaType>
#include <QString>

class Country {
public:
    Country();
    Country(const Country& country);
    Country(const QString& name,
            const QString& capital,
            int population,
            const QString& continent);
    ~Country();

    QString& GetName();
    void SetName(const QString& value);

    QString& GetCapital();
    void SetCapital(const QString& value);

    int GetPopulation();
    void SetPopulation(int value);

    QString& GetContinent();
    void SetContinent(const QString& value);

private:
    QString _name;
    QString _capital;
    int     _population;
    QString _continent;
};

Q_DECLARE_METATYPE(Country)

#endif // COUNTRY_H

