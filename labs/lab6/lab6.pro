#-------------------------------------------------
#
# Project created by QtCreator 2017-06-07T12:05:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab6
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    country.cpp \
    countrydialog.cpp

HEADERS  += mainwindow.h \
    country.h \
    countrydialog.h

FORMS    += mainwindow.ui \
    countrydialog.ui
