#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "countrydialog.h"

#include <QDesktopWidget>

void FillSampleData(QVector<Country>& countries) {
    countries.push_back(Country("Germany", "Berlin", 45000000,  "Europe"));
    countries.push_back(Country("Nepal", "Kathmandu", 36240000,  "Asia"));
    countries.push_back(Country("Japan", "Tokyo", 28675000, "Asia"));
    countries.push_back(Country("Egypt", "Cairo", 43980000, "Africa"));
    countries.push_back(Country("Pakistan", "Islamabad", 56912000, "Asia"));
    countries.push_back(Country("Estonia", "Tallinn", 19453000, "Europe"));
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // center to screen
    setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            size(),
            QApplication::desktop()->availableGeometry()
        )
    );

    _buttonRemove         = findChild<QPushButton*>("buttonRemove");
    _lineEditContinent    = findChild<QLineEdit*>("lineEditContinent");
    _listWidgetCountries  = findChild<QListWidget*>("listWidgetCountry");
    _textEditFindResult   = findChild<QTextEdit*>("textEditFindResult");
    _labelNameValue       = findChild<QLabel*>("labelNameValue");
    _labelCapitalValue    = findChild<QLabel*>("labelCapitalValue");
    _labelPopulationValue = findChild<QLabel*>("labelPopulationValue");
    _labelContinentValue  = findChild<QLabel*>("labelContinentValue");

    QVector<Country> countries;
    FillSampleData(countries);
    for (int n = 0; n < countries.count(); n++) {
        Country country = countries[n];
        QVariant var;
        var.setValue(country);
        QListWidgetItem* pItem = new QListWidgetItem();
        pItem->setText(country.GetName());
        pItem->setData(Qt::UserRole, var);
        _listWidgetCountries->addItem(pItem);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonAdd_clicked()
{
    Country country;
    CountryDialog dlg;
    dlg.SetCountry(&country);
    if (dlg.exec()) {
        QVariant var;
        var.setValue(country);
        QListWidgetItem* pItem = new QListWidgetItem();
        pItem->setText(country.GetName());
        pItem->setData(Qt::UserRole, var);
        _listWidgetCountries->addItem(pItem);
    }
}

void MainWindow::on_buttonRemove_clicked()
{
    QListWidgetItem* pItem = _listWidgetCountries->currentItem();
    if (pItem != NULL)
        delete pItem;
}

void MainWindow::on_buttonFind_clicked()
{
    QString continent = _lineEditContinent->text();
    if (continent.isEmpty())
        return;

    _textEditFindResult->setText("");
    for(int n = 0; n < _listWidgetCountries->count(); n++) {
        QListWidgetItem *pItem = _listWidgetCountries->item(n);
        Country country = pItem->data(Qt::UserRole).value<Country>();
        if (country.GetContinent() == continent)
            _textEditFindResult->append(country.GetName());
    }
}

void MainWindow::on_listWidgetCountry_currentItemChanged(QListWidgetItem *current, QListWidgetItem *)
{
    _buttonRemove->setEnabled(current != NULL);
    if (current != NULL) {
        Country country = current->data(Qt::UserRole).value<Country>();
        _labelNameValue->setText(country.GetName());
        _labelCapitalValue->setText(country.GetCapital());
        _labelPopulationValue->setText(QString("%1").arg(country.GetPopulation()));
        _labelContinentValue->setText(country.GetContinent());

    } else {
        _labelNameValue->setText("");
        _labelCapitalValue->setText("");
        _labelPopulationValue->setText("");
        _labelContinentValue->setText("");
    }
}
