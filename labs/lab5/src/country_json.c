///////////////////////////////////////////////////////////////////////////////
// country_json.c
//

#include <jansson.h>

#include "country_json.h"

#define J_COUNTRIES     "Countries"
#define J_CAPITAL       "Capital"
#define J_NAME          "name"
#define J_LATITUDE      "latitude"
#define J_POPULATION    "population"

#define INT_FORMAT      "%d"
#define DOUBLE_FORMAT   "%lf"

int ReadCountriesFromJson(CountryList countryList, const char* fileName) {
  int n;
  Country country;
  json_t* rootNode;
  json_t* countriesNode;
  json_t* countryNode;
  json_t* capitalNode;

  rootNode = json_load_file(fileName, 0, NULL);
  if (rootNode == NULL)
    return -1;
  countriesNode = json_object_get(rootNode, J_COUNTRIES);
  if (countriesNode == NULL || !json_is_array(countriesNode)) {
    json_decref(rootNode);
    return -1;
  }
  
  for(n = 0; n < json_array_size(countriesNode); n++) {
    countryNode = json_array_get(countriesNode, n);
    if (!json_is_object(countryNode))
      continue;
    strcpy(&country.name, json_string_value(json_object_get(countryNode, J_NAME)));
    country.latitude = json_integer_value(json_object_get(countryNode, J_LATITUDE));
    country.population = json_real_value(json_object_get(countryNode, J_POPULATION));
    capitalNode = json_object_get(countryNode, J_CAPITAL);
    if (capitalNode == NULL || !json_is_object(capitalNode))
      continue;
    strcpy(&country.capital.name, json_string_value(json_object_get(capitalNode, J_NAME)));
    country.capital.population = json_real_value(json_object_get(capitalNode, J_POPULATION));
    AddCountry(countryList, country);
  }
  json_decref(rootNode);
  return 0;
}

int SaveCountriesToJson(CountryList countryList, const char* fileName) {
  int n;
  Country country;
  json_t* rootNode;
  json_t* countriesNode;
  json_t* countryNode;
  json_t* capitalNode;

  rootNode = json_object();
  countriesNode = json_array();
  json_object_set_new(rootNode, J_COUNTRIES, countriesNode);
  for (n = 0; n < GetCountryCount(countryList); n++) {
    country = GetCountry(countryList, n);
    countryNode = json_object();
    json_object_set_new(countryNode, J_NAME,       json_string(country.name));
    json_object_set_new(countryNode, J_LATITUDE,   json_integer(country.latitude));
    json_object_set_new(countryNode, J_POPULATION, json_real(country.population));
    capitalNode = json_object();
    json_object_set_new(capitalNode, J_NAME,       json_string(country.capital.name));
    json_object_set_new(capitalNode, J_POPULATION, json_real(country.capital.population));
    json_object_set_new(countryNode, J_CAPITAL,    capitalNode);
    json_array_append(countriesNode, countryNode);
  }
  n = json_dump_file(rootNode, fileName, 0);
  json_decref(rootNode);
  return n;
}
