///////////////////////////////////////////////////////////////////////////////
// country_xml.c
//

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "country_xml.h"

#define MAX_RECOD_SIZE  255

#define XML_VER         "1.0"

#define NODE_COUNTRIES  "Countries"
#define NODE_COUNTRY    "Country"
#define NODE_CAPITAL    "Capital"
#define ATTR_NAME       "name"
#define ATTR_LATITUDE   "latitude"
#define ATTR_POPULATION "population"

#define INT_FORMAT      "%d"
#define DOUBLE_FORMAT   "%lf"

int ReadCountriesFromXML(CountryList countryList, const char* fileName) {
  char* s;
  Country country;
  xmlDoc* doc = NULL;
  xmlNode* rootNode = NULL;
  xmlNode* countryNode = NULL;
  xmlNode* countryCapitalNode = NULL;

  doc = xmlReadFile(fileName, NULL, 0);
  if (doc == NULL)
    return -1;
  rootNode = xmlDocGetRootElement(doc);
  if (rootNode == NULL || strcmp(rootNode->name, NODE_COUNTRIES) != 0) {
    xmlFreeDoc(doc);
    return -1;
  }

  ClearCountries(countryList);
  for(countryNode = rootNode->children; countryNode != NULL ; countryNode = countryNode->next) {
    if (countryNode->type != XML_ELEMENT_NODE || strcmp(countryNode->name, NODE_COUNTRY) != 0)
      continue;
    s = xmlGetProp(countryNode, ATTR_NAME);
    strcpy(&country.name, s);
    xmlFree(s);
    s = xmlGetProp(countryNode, ATTR_LATITUDE);
    sscanf(s, INT_FORMAT, &country.latitude);
    xmlFree(s);
    s = xmlGetProp(countryNode, ATTR_POPULATION);
    sscanf(s, DOUBLE_FORMAT, &country.population);
    xmlFree(s);
    for(countryCapitalNode = countryNode->children; countryCapitalNode != NULL ; countryCapitalNode = countryCapitalNode->next)
      if (countryCapitalNode->type == XML_ELEMENT_NODE && strcmp(countryCapitalNode->name, NODE_CAPITAL) == 0)
        break;
    if (countryCapitalNode == NULL)
      continue;
    s = xmlGetProp(countryCapitalNode, ATTR_NAME);
    strcpy(&country.capital.name, s);
    xmlFree(s);
    s = xmlGetProp(countryCapitalNode, ATTR_POPULATION);
    sscanf(s, DOUBLE_FORMAT, &country.capital.population);
    xmlFree(s);
    AddCountry(countryList, country);
  }
  xmlFreeDoc(doc);
  return 0;
}

int SaveCountriesToXML(CountryList countryList, const char* fileName) {
  int n;
  char s[MAX_RECOD_SIZE];
  Country country;
  xmlDoc* doc = NULL;
  xmlNode* rootNode = NULL;
  xmlNode* countryNode = NULL;
  xmlNode* countryCapitalNode = NULL;

  doc = xmlNewDoc(XML_VER);
  rootNode = xmlNewNode(NULL, NODE_COUNTRIES);
  xmlDocSetRootElement(doc, rootNode);
  for (n = 0; n < GetCountryCount(countryList); n++) {
    country = GetCountry(countryList, n);
    countryNode = xmlNewChild(rootNode, NULL, NODE_COUNTRY, NULL);
    xmlNewProp(countryNode, ATTR_NAME, country.name);
    sprintf(s, INT_FORMAT, country.latitude);    
    xmlNewProp(countryNode, ATTR_LATITUDE, s);
    sprintf(s, DOUBLE_FORMAT, country.population);
    xmlNewProp(countryNode, ATTR_POPULATION, s);
    countryCapitalNode = xmlNewChild(countryNode, NULL, NODE_CAPITAL, NULL);
    xmlNewProp(countryCapitalNode, ATTR_NAME, country.capital.name);
    sprintf(s, DOUBLE_FORMAT, country.capital.population);
    xmlNewProp(countryCapitalNode, ATTR_POPULATION, s);
  }
  n = xmlSaveFile(fileName, doc);
  xmlFreeDoc(doc);
  return n;
}
