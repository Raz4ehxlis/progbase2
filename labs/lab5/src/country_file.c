///////////////////////////////////////////////////////////////////////////////
// country_file.c
//

#include <stdio.h>

#include "country_file.h"

#define MAX_RECOD_SIZE 255

int ReadCountriesFromFile(CountryList countryList, const char* fileName) {
  Country country;
  char s[MAX_RECOD_SIZE];
  int len = MAX_RECOD_SIZE;

  FILE* f = fopen(fileName, "r");
  if (f == 0)
    return 1;

  ClearCountries(countryList);
  while (!feof(f)) {
    if (fgets(s, len, f) == 0)
      break;
    country = ReadCountryFromString(s);
    AddCountry(countryList, country);
  }

  fclose(f);
  return 0;
}

int SaveCountriesToFile(CountryList countryList, const char* fileName) {
  int n;
  char s[255];

  FILE* f = fopen(fileName, "w");
  if (f == 0)
    return 1;

  for (n = 0; n < GetCountryCount(countryList); n++) {
    SaveCountryToString(GetCountry(countryList, n), s);
    fprintf(f, "%s\n", s);
  }

  fclose(f);
  return 0;
}
