# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/student/lab5/src/country.c" "/home/student/lab5/CMakeFiles/libLab5.dir/src/country.c.o"
  "/home/student/lab5/src/country_file.c" "/home/student/lab5/CMakeFiles/libLab5.dir/src/country_file.c.o"
  "/home/student/lab5/src/country_json.c" "/home/student/lab5/CMakeFiles/libLab5.dir/src/country_json.c.o"
  "/home/student/lab5/src/country_list.c" "/home/student/lab5/CMakeFiles/libLab5.dir/src/country_list.c.o"
  "/home/student/lab5/src/country_xml.c" "/home/student/lab5/CMakeFiles/libLab5.dir/src/country_xml.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/usr/include/libxml2"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
