///////////////////////////////////////////////////////////////////////////////
// country.h
//

#ifndef COUNTRY_H
#define COUNTRY_H

#define NAME_SIZE 20

struct Country {
  char   name[NAME_SIZE];
  int    latitude;
  double population;
  struct Capital {
    char name[NAME_SIZE];
    double population;
  } capital;
};

typedef struct Country Country;

Country ReadCountryFromString(const char* s);
void SaveCountryToString(Country country, char* s);

#endif
