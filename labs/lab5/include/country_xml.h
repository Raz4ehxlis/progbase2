///////////////////////////////////////////////////////////////////////////////
// country_xml.h
//

#ifndef COUNTRY_XML_H
#define COUNTRY_XML_H

#include "country_list.h"

int ReadCountriesFromXML(CountryList countryList, const char* fileName);
int SaveCountriesToXML(CountryList countryList, const char* fileName);

#endif
