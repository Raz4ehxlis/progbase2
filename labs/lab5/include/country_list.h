///////////////////////////////////////////////////////////////////////////////
// country_list.h
//

#ifndef COUNTRY_LIST_H
#define COUNTRY_LIST_H

#include "country.h"

typedef void* CountryList;

CountryList CreateCountryList();
void DeleteCountryList(CountryList countryList);

void ClearCountries(CountryList countryList);
void AddCountry(CountryList countryList, Country country);
void InsertCountry(CountryList countryList, Country country, int Index);
void DeleteCountry(CountryList countryList, int Index);
int GetCountryCount(CountryList countryList);
Country GetCountry(CountryList countryList, int Index);
void SetCountry(CountryList countryList, Country country, int Index);

void FindNorthCountries(CountryList countryList, CountryList northCountryList);

#endif
