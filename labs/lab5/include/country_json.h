///////////////////////////////////////////////////////////////////////////////
// country_json.h
//

#ifndef COUNTRY_JSON_H
#define COUNTRY_JSON_H

#include "country_list.h"

int ReadCountriesFromJson(CountryList countryList, const char* fileName);
int SaveCountriesToJson(CountryList countryList, const char* fileName);

#endif
