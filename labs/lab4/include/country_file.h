///////////////////////////////////////////////////////////////////////////////
// country_file.h
//

#ifndef COUNTRY_FILE_H
#define COUNTRY_FILE_H

#include "country_list.h"

int ReadCountriesFromFile(CountryList countryList, const char* fileName);
int SaveCountriesToFile(CountryList countryList, const char* fileName);

#endif
