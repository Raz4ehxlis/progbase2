#include <stdlib.h>
#include <check.h>
#include <string.h>

#include "country_list.h"
#include "country_file.h"
#include "country.h"


START_TEST (test_GetCountryCount)
{ 
    CountryList self = CreateCountryList();
    ck_assert_int_eq(GetCountryCount(self), 0);
    ReadCountriesFromFile(self, "c.txt");
    ck_assert_int_eq(GetCountryCount(self), 5);
}
END_TEST

START_TEST(test_GetCountry)
{
    CountryList self = CreateCountryList();
    ReadCountriesFromFile(self, "c.txt");
    char s[255];    
    SaveCountryToString(GetCountry(self, 2), &s[0]);    
    ck_assert_str_eq( s, "Egypt\t33\t3.600000\tKair\t0.500000");
}
END_TEST

START_TEST (test_ReadCountriesFromFile)
{
  CountryList self = CreateCountryList();
  ck_assert_int_eq(ReadCountriesFromFile(self, "c.txt"), 0);
  ck_assert_int_eq(ReadCountriesFromFile(self, "b.txt"), 1);
}
END_TEST

START_TEST (test_SaveCountriesToFile)
{
  CountryList self = CreateCountryList();
  ck_assert_int_eq(SaveCountriesToFile(self, "test.txt"), 0);  
  remove("test.txt");
}
END_TEST

Suite * test_suite() {

  Suite * s = suite_create("mcountry");
  TCase * tc_sample;
  tc_sample = tcase_create("TestCountry");

  tcase_add_test(tc_sample, test_SaveCountriesToFile);
  tcase_add_test(tc_sample, test_ReadCountriesFromFile);
  tcase_add_test(tc_sample, test_GetCountry);
  tcase_add_test(tc_sample, test_GetCountryCount);
  suite_add_tcase(s, tc_sample);

  return s;
}

int main(void) {
    
    Suite *s = test_suite();
  	SRunner *sr = srunner_create(s);
  	srunner_set_fork_status(sr, CK_NOFORK);
 
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

