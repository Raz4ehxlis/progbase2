///////////////////////////////////////////////////////////////////////////////
// country.c
//

#include <stdio.h>

#include "country.h"

#define COUNTRY_FORMAT "%s\t%d\t%lf\t%s\t%lf"

Country ReadCountryFromString(const char* s) {
  Country country;

  sscanf(s, COUNTRY_FORMAT,
    &country.name,
    &country.latitude,
    &country.population,
    &country.capital.name,
    &country.capital.population
  );
  return country;
}

void SaveCountryToString(Country country, char* s) {
  sprintf(s, COUNTRY_FORMAT,
    country.name,
    country.latitude,
    country.population,
    country.capital.name,
    country.capital.population
  );
}
