///////////////////////////////////////////////////////////////////////////////
// country_list.c
//

#include <stdio.h>
#include <malloc.h>

#include "country_list.h"

struct InternalCountryList {
  Country* Countries;
  int Count;
};

typedef struct InternalCountryList* _CountryList;

CountryList CreateCountryList() {
  _CountryList _countryList = (_CountryList) malloc(sizeof(struct InternalCountryList));

  _countryList->Countries = 0;
  _countryList->Count = 0;
  return _countryList;
}

void DeleteCountryList(CountryList countryList) {
  ClearCountries(countryList);
  free(countryList);
}

void ClearCountries(CountryList countryList) {
  _CountryList _countryList = (_CountryList) countryList;

  if(_countryList->Countries != 0)
    free(_countryList->Countries);
  _countryList->Countries = 0;
  _countryList->Count = 0;
}

void AddCountry(CountryList countryList, Country country) {
  InsertCountry(countryList, country, GetCountryCount(countryList));
}

void InsertCountry(CountryList countryList, Country country, int Index) {
  int n;
  _CountryList _countryList = (_CountryList) countryList;
  Country* oldCountries = _countryList->Countries;

  _countryList->Countries = (Country*) malloc((_countryList->Count + 1) * sizeof(Country));
  for (n = 0; n < _countryList->Count + 1; n++) {
    if (n < Index)
      _countryList->Countries[n] = oldCountries[n];
    else if (n == Index)
      _countryList->Countries[n] = country;
    else  // n > Index
      _countryList->Countries[n] = oldCountries[n - 1];
  }
  free(oldCountries);
  _countryList->Count += 1;
}

void DeleteCountry(CountryList countryList, int Index) {
  int n;
  _CountryList _countryList = (_CountryList) countryList;
  Country* oldCountries = _countryList->Countries;

  _countryList->Countries = (Country*) malloc((_countryList->Count - 1) * sizeof(Country));
  for (n = 0; n < _countryList->Count - 1; n++) {
    if (n < Index)
      _countryList->Countries[n] = oldCountries[n];
    else // n > Index
      _countryList->Countries[n] = oldCountries[n + 1];
  }
  free(oldCountries);
  _countryList->Count -= 1;
}

int GetCountryCount(CountryList countryList) {
  _CountryList _countryList = (_CountryList) countryList;

  return _countryList->Count;
}

Country GetCountry(CountryList countryList, int Index) {
  _CountryList _countryList = (_CountryList) countryList;
  
  return _countryList->Countries[Index];
}

void SetCountry(CountryList countryList, Country country, int Index) {
  _CountryList _countryList = (_CountryList) countryList;

  _countryList->Countries[Index] = country;
}

void FindNorthCountries(CountryList countryList, CountryList nordCountryList) {
  int n;
  _CountryList _countryList = (_CountryList) countryList;

  ClearCountries(nordCountryList);
  for(n = 0; n < _countryList->Count; n++)
    if(_countryList->Countries[n].latitude > 0)
      AddCountry(nordCountryList, _countryList->Countries[n]);
}
