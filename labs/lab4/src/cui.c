///////////////////////////////////////////////////////////////////////////////
// cui.c
//

#include <stdio.h>

#include "country.h"
#include "country_list.h"
#include "country_file.h"

#define MAX_RECOD_SIZE 255

#define MENU_EXIT                 0
#define MENU_LOAD_FROM_FILE       1
#define MENU_SAVE_TO_FILE         2
#define MENU_PRINT_COUNTRY_LIST   3
#define MENU_CLEAR_COUNTRY_LIST   4
#define MENU_INSERT_COUNTY        5
#define MENU_DELETE_COUNTRY       6
#define MENU_EDIT_COUNTRY         7
#define MENU_FIND_NORD_COUNTRIES  8

void PrintUsage() {
  printf("Please select:\n"
         "0. Exit.\n"
         "1. Load country list from file.\n"
         "2. Save country list to file.\n"
         "3. Print country list.\n"
         "4. Clear country list.\n"
         "5. Insert new country to country list.\n"
         "6. Delete country from country list.\n"
         "7. Edit country in country list.\n"
         "8. Print north countries.\n"
         "Make a choice: ");
}

#define MENU_EDIT_COUNTRY_NAME                1
#define MENU_EDIT_COUNTRY_LATITUDE            2
#define MENU_EDIT_COUNTRY_POPULATION          3
#define MENU_EDIT_COUNTRY_CAPITAL_NAME        4
#define MENU_EDIT_COUNTRY_CAPITAL_POPULATION  5

void PrintCountryEditUsage() {
  printf("Please select:\n"
         "0. Exit from edit country.\n"
         "1. Change name.\n"
         "2. Change latitude.\n"
         "3. Change population.\n"
         "4. Change capital name.\n"
         "5. Change capital population.\n"
         "Make a choice: ");
}

int InputInt() {
  int n;
  scanf("%d", &n);
  return n;
}

double InputDouble() {
  double d;
  scanf("%lf", &d);
  return d;
}

void InputString(char* s) {
  scanf("%s", s);
}

void LoadFromFile(CountryList countryList) {
  char fileName[MAX_RECOD_SIZE];

  printf("Enter name of file: ");
  InputString(fileName);
  if (ReadCountriesFromFile(countryList, fileName) == 0)
    printf("  Done.\n");
  else
    printf("  Error.\n");
}

void SaveToFile(CountryList countryList) {
  char fileName[MAX_RECOD_SIZE];

  printf("Enter name of file: ");
  InputString(fileName);
  if (SaveCountriesToFile(countryList, fileName) == 0)
    printf("  Done.\n");
  else
    printf("  Error.\n");
}

void PrintCountryList(const char* title, CountryList countryList) {
  int n;
  char s[MAX_RECOD_SIZE];
  int count = GetCountryCount(countryList);

  printf("%s\n", title);
  if (count > 0) {
    for (n = 0; n < GetCountryCount(countryList); n++) {
      SaveCountryToString(GetCountry(countryList, n), s);
      printf("%d. %s\n", n, s);
    }
  }
  else
    printf("  empty\n");
}

void ClearCountryList(CountryList countryList) {
  ClearCountries(countryList);
  printf("  Country list cleared.\n");
}

void InsertCountryToList(CountryList countryList) {
  int index = -1;
  int count = GetCountryCount(countryList);
  Country country;

  printf("Enter country name: ");
  InputString(&country.name[0]);

  printf("Enter country latitude: ");
  country.latitude = InputInt();

  printf("Enter country population: ");
  country.population = InputDouble();

  printf("Enter country capital name: ");
  InputString(&country.capital.name[0]);

  printf("Enter country capital population: ");
  country.capital.population = InputDouble();

  while (index < 0 || index > count) {
    printf("Enter country index in cuntry list (0-%d): ", count);
    index = InputInt();
  }

  InsertCountry(countryList, country, index);
  printf("  Country \"%s\" added to country list at index %d.\n", country.name, index);
}

void DeleteCountryFromList(CountryList countryList) {
  int index = -1;
  int count = GetCountryCount(countryList);

  PrintCountryList("Country list:", countryList);
  if (count == 0)
    return;

  while (index < 0 || index > count -1) {
    printf("Select country index to delete: ");
    index = InputInt();
  }

  DeleteCountry(countryList, index);
  printf("  Country at index %d deleted from country list.\n", index);
}

void EditCountry(CountryList countryList) {
  Country country;
  int index = -1;
  char s[MAX_RECOD_SIZE];
  int count = GetCountryCount(countryList);

  PrintCountryList("Country list:", countryList);
  if (count == 0)
    return;

  while (index < 0 || index > count -1) {
    printf("Select country index to edit: ");
    index = InputInt();
  }

  country = GetCountry(countryList, index);

  while (1) {
    PrintCountryEditUsage();
    switch (InputInt()) {
      case MENU_EXIT:
        SetCountry(countryList, country, index);
        SaveCountryToString(country, s);
        printf("  Country at index %d changed in country list.\n", index);
        printf("    %s\n", s);
        return;
      case MENU_EDIT_COUNTRY_NAME:
        printf("Enter country name: ");
        InputString(&country.name[0]);
        break;
      case MENU_EDIT_COUNTRY_LATITUDE:
        printf("Enter country latitude: ");
        country.latitude = InputInt();
        break;
      case MENU_EDIT_COUNTRY_POPULATION:
        printf("Enter country population: ");
        country.population = InputDouble();
        break;
      case MENU_EDIT_COUNTRY_CAPITAL_NAME:
        printf("Enter country capital name: ");
        InputString(&country.capital.name[0]);
        break;
      case MENU_EDIT_COUNTRY_CAPITAL_POPULATION:
        printf("Enter country capital population: ");
        country.capital.population = InputDouble();
        break;
    }
  }
}

void FindNorthCountriesInList(CountryList countryList) {
  CountryList northCountries = CreateCountryList();

  FindNorthCountries(countryList, northCountries);
  PrintCountryList("North countries:", northCountries);
  DeleteCountryList(northCountries);
}

void MainMenu() {
  CountryList countryList = CreateCountryList();

  while (1) {
    PrintUsage();
    switch (InputInt()) {
      case MENU_EXIT:
        DeleteCountryList(countryList);
        return;
      case MENU_LOAD_FROM_FILE:
        LoadFromFile(countryList);
        break;
      case MENU_SAVE_TO_FILE:
        SaveToFile(countryList);
        break;
      case MENU_PRINT_COUNTRY_LIST:
        PrintCountryList("Country list:", countryList);
        break;
      case MENU_CLEAR_COUNTRY_LIST:
        ClearCountryList(countryList);
        break;
      case MENU_INSERT_COUNTY:
        InsertCountryToList(countryList);
        break;
      case MENU_DELETE_COUNTRY:
        DeleteCountryFromList(countryList);
        break;
      case MENU_EDIT_COUNTRY:
        EditCountry(countryList);
        break;
      case MENU_FIND_NORD_COUNTRIES:
        FindNorthCountriesInList(countryList);
        break;
    }
    printf("\n");
  }
}
